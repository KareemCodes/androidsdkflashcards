package cs.mad.flashcards.entities

import androidx.room.*

@Entity
data class FlashcardSet(val title: String, val id: Long? = null)

@Dao
interface FlashcardSetDAO {
    @Query("select * from flashcardSet order by lower(title) asc")
    fun getAll(): List<Flashcard>

    @Query("delete from flashcardSet where id NOT NULL")
    fun deleteFromWeb()

    @Insert
    fun insert(vararg flashcard: FlashcardSet)

    @Insert
    fun insert(flashcardSets: List<FlashcardSet>)

    @Update
    fun update(flashcardSet: FlashcardSet)

    @Delete
    fun delete(flashcardSet: FlashcardSet)
}


/*fun getHardcodedFlashcardSets(): List<FlashcardSet> {
    return mutableListOf(FlashcardSet("Set 1"),
        FlashcardSet( "Set 2"),
        FlashcardSet( "Set 3"),
        FlashcardSet( "Set 4"),
        FlashcardSet( "Set 5"),
        FlashcardSet( "Set 6"),
        FlashcardSet( "Set 7"),
        FlashcardSet( "Set 8"),
        FlashcardSet( "Set 9"),
        FlashcardSet( "Set 10")
    )

 */