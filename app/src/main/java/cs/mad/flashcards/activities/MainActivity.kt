package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
//import cs.mad.flashcards.entities.getHardcodedFlashcardSets

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //binding.flashcardSetList.adapter = FlashcardSetAdapter(getHardcodedFlashcardSets())

        binding.createSetButton.setOnClickListener {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(FlashcardSet("test"))
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
        }
    }
}
        /*flashcardService = Retrofit.Builder()
                .baseUrl("https://age-of-empires-2-api.herokuapp.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(CivilizationService::class.java)

        setupRecycler()
        setupButtons()
        loadCivs()
    }
    private fun setupRecycler() {
        binding.recyclerView.adapter = FlashcardAdapter(listOf(), flashcardDao)
    }

    private fun setupButtons() {
        binding.doOnceButton.setOnClickListener {
            val preferences = getSharedPreferences("MY_PREFS", MODE_PRIVATE)
            if (!(preferences.getBoolean("HAVE_DONE", false))) {
                preferences.edit().putBoolean("HAVE_DONE", true).apply()
                AlertDialog.Builder(this)
                        .setMessage("Can only be shown once...ever")
                        .setPositiveButton("Okay") { _, _ -> }
                        .create()
                        .show()
            }
        }

        binding.saveTitleButton.setOnClickListener {
            lifecycleScope.launch {
                flashcardDao.insert(
                        Flashcard(
                                null,
                                null,
                                binding.newTitle.text.toString(),
                                "Custom"
                        )
                )
                binding.newTitle.setText("")
                loadFromDb()
            }
        }
    }

    private fun loadCivs() {
        loadFromDb()
        lifecycleScope.launch {
            flashcardService.getAll().enqueue(object : Callback<Flashcards> {
                override fun onResponse(
                        call: Call<Flashcards>,
                        response: Response<Flashcards>
                ) {
                    val result = response.body()
                    if (result != null) {
                        lifecycleScope.launch {
                            FlashcardDao.deleteFromWeb()
                            FlashcardDao.insert(result.civilizations)
                            loadFromDb()
                        }
                    }
                }

                override fun onFailure(call: Call<WebFlashcards>, t: Throwable) {}
            })
        }
    }

    private fun loadFromDb() {
        lifecycleScope.launch {
            (binding.recyclerView.adapter as FlashcardAdapter).setData(FlashcardDao.getAll())
        }
    }
*/
