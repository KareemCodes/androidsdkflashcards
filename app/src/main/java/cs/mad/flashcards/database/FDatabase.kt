package cs.mad.flashcards.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDAO
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDAO

class FDatabase {

    @Database(entities = [Flashcard::class, FlashcardSet::class], version = 1)
    abstract class FDatabase: RoomDatabase() {
        abstract fun flashcardDao(): FlashcardDAO
        abstract fun flashcardSetDao(): FlashcardSetDAO

        companion object {
            // Singleton prevents multiple instances of database opening at the
            // same time.
            @Volatile
            private var INSTANCE: FDatabase? = null

            private fun getDatabase(context: Context): FDatabase {
                // if the INSTANCE is not null, then return it,
                // if it is, then create the database
                return INSTANCE ?: synchronized(this) {
                    val instance = Room.databaseBuilder(
                            context.applicationContext,
                            getDatabase(context)::class.java,
                            "app_database"
                    ).fallbackToDestructiveMigration().build()
                    INSTANCE = instance
                    // return instance
                    instance
                }
            }
        }

    }
}